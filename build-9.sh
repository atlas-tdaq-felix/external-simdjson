#!/bin/sh -x

set -e

SVERSION=3.1.6
DVERSION=3.1.6
PLATFORM=x86_64-centos9-gcc11-opt

g++ -Wall -O3 -g -pedantic -O3 -mtune=native -ftree-vectorize -fomit-frame-pointer -fPIC -std=c++17 -c -o simdjson.o -Isimdjson-${SVERSION}/singleheader/ simdjson-${SVERSION}/singleheader/simdjson.cpp
g++ -shared -o libsimdjson.so simdjson.o

rm simdjson.o
mkdir -p ${DVERSION}/${PLATFORM}/include
cp simdjson-${SVERSION}/singleheader/simdjson.h ${DVERSION}/${PLATFORM}/include/
mkdir -p ${DVERSION}/${PLATFORM}/lib
mv libsimdjson.so ${DVERSION}/${PLATFORM}/lib/
